﻿
using Apitron.PDF.Kit;
using Aspose.Pdf;
using SautinSoft.Document;
using Spire.Pdf;
using Syncfusion.Pdf.Parsing;
using System;
using System.Drawing;
using System.IO;
using System.Net.WebSockets;
using System.Reflection.Metadata.Ecma335;


namespace ConvertPDFA
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var sourcePath = @"C:\Users\User\Desktop\ConvertPDFA\piano.pdf";

            byte[] fileBytes = System.IO.File.ReadAllBytes(sourcePath);


            //SpirePDF(fileBytes);
            //Itext();  // Chưa chạy
            //Sautinsoft(fileBytes);
            //Apitron(sourcePath);
            //Aspose(sourcePath); //Chưa chạy
        }

        /// <summary>
        /// Output PDF/A-1A/1B/2A
        /// </summary>
        /// <param name="fileBytes"></param>
        private static void SpirePDF(byte[] fileBytes)
        {
            var desPath = @"C:\Users\vietdh\Desktop\ConvertPDFA\spire.pdf";

            var begin = DateTime.Now;

            PdfDocument OriginalDoc = new PdfDocument();
            OriginalDoc.LoadFromBytes(fileBytes);

            PdfNewDocument newDOC = new PdfNewDocument();
            newDOC.Conformance = PdfConformanceLevel.Pdf_A1B;

            foreach (PdfPageBase page in OriginalDoc.Pages)
            {
                SizeF size = page.Size;
                PdfPageBase p = newDOC.Pages.Add(size, new Spire.Pdf.Graphics.PdfMargins(0));
                page.CreateTemplate().Draw(p, 0, 0);
            }

            using (var stream = new FileStream(path: desPath, mode: FileMode.Create, access: FileAccess.Write, share: FileShare.Read))
            {
                newDOC.Save(stream);
            }

            var finish = DateTime.Now;

            Console.WriteLine("Convert with SpirePDF Successfully. Total Time: " + (finish - begin).TotalMilliseconds);

            return;
        }

        private static void IText()
        {
        }

        /// <summary>
        /// Output PDFA-3A
        /// </summary>
        /// <param name="fileBytes"></param>
        private static void Sautinsoft(byte[] fileBytes)
        {
            var desPath = @"C:\Users\vietdh\Desktop\ConvertPDFA\sautinsoft.pdf";
            byte[] outData = null;
            var begin = DateTime.Now;

            using (MemoryStream msInp = new MemoryStream(fileBytes))
            {
                // Specifying PdfLoadOptions we explicitly set that a loadable document is PDF.
                PdfLoadOptions pdfLO = new PdfLoadOptions()
                {
                    // 'false' - means to load vector graphics as is. Don't transform it to raster images.
                    RasterizeVectorGraphics = false,

                    // The PDF format doesn't have real tables, in fact it's a set of orthogonal graphic lines.
                    // In case of 'true' the component will detect and recreate tables from graphic lines.
                    DetectTables = false,

                    // 'true' - Load embedded fonts from PDF document, even if the font with the same name is installed in your System.
                    PreserveEmbeddedFonts = false
                };

                // Load a document.
                DocumentCore dc = DocumentCore.Load(msInp, pdfLO);

                // Save the document to PDF/A format.
                SautinSoft.Document.PdfSaveOptions pdfSO = new SautinSoft.Document.PdfSaveOptions()
                {
                    Compliance = PdfCompliance.PDF_A
                };

                using (MemoryStream outMs = new MemoryStream())
                {
                    dc.Save(outMs, pdfSO);
                    outData = outMs.ToArray();
                }

                // Show the result for demonstration purposes.
                if (outData != null)
                {
                    File.WriteAllBytes(desPath, outData);
                }
            }

            var finish = DateTime.Now;

            Console.WriteLine("Convert with Sautinsoft Successfully. Total Time: " + (finish - begin).TotalMilliseconds);

            return;
        }

        /// <summary>
        /// Output: PDF/A-1A
        /// </summary>
        /// <param name="sourcePath"></param>
        private static void Apitron(string sourcePath)
        {
            var begin = DateTime.Now;
            var desPath = @"C:\Users\vietdh\Desktop\ConvertPDFA\apitron.pdf";

            using (Stream inputStream = File.Open(sourcePath, FileMode.Open),
                outputStream = File.Create(desPath))
            {
                using (FixedDocument pdfDocument = new FixedDocument(inputStream, PdfStandard.PDFA))
                {
                    // ... you can add or edit the contents of the PDF file here
                    pdfDocument.Save(outputStream);
                }

                var finish = DateTime.Now;

                Console.WriteLine("Convert with Apitron Successfully. Total Time: " + (finish - begin).TotalMilliseconds);

                return;
            }
        }

        /// <summary>
        /// Output : All 2.5M installed
        /// </summary>
        private static void Aspose(string sourcePath)
        {
            var begin = DateTime.Now;
            var desPath = @"C:\Users\User\Desktop\ConvertPDFA\aspose.pdf";

            using (var stream = new FileStream(path: sourcePath, mode: FileMode.Open))
            {
                // Open document
                Document pdfDocument = new Document(stream);

                // Convert to PDF/A compliant document
                // During conversion process, the validation is also performed

                pdfDocument.Convert(new MemoryStream(), PdfFormat.PDF_A_1A, ConvertErrorAction.None, ConvertTransparencyAction.Default);

                var streamDes = new FileStream(path: desPath, mode: FileMode.Create);
                // Save output document
                pdfDocument.Save(streamDes);

            }

            var finish = DateTime.Now;

            Console.WriteLine("Convert with Apitron Successfully. Total Time: " + (finish - begin).TotalMilliseconds);

            return;
        }


        /// <summary>
        /// Output: PDF/A-1A 1B 2A
        /// </summary>
        /// <param name="sourcePath"></param>
        private static void Syncfusion(string sourcePath)
        {
            var begin = DateTime.Now;
            var desPath = @"C:\Users\vietdh\Desktop\ConvertPDFA\apitron.pdf";


            using (Stream inputStream = File.Open(sourcePath, FileMode.Open),
                outputStream = File.Create(desPath))
            {
                //Load an existing PDF
                PdfLoadedDocument loadedDocument = new PdfLoadedDocument(inputStream);

                //Set the conformance for PDF/A-1b conversion
                loadedDocument.Conformance = PdfConformanceLevel.Pdf_A1A

                //Save the document
                loadedDocument.Save("Output.pdf");

                //Close the document
                loadedDocument.Close(true);
            }

            

          

            //This will open the PDF file so, the result will be seen in default PDF viewer
            System.Diagnostics.Process.Start("Output.pdf");
        }
    }
}
